import React, { useEffect, useState } from 'react'
import { Table, Row, Col, Form, Group, Label, Control, Button, Text, Image } from 'react-bootstrap';
import {
    postAuthor,
    uploadImage,
    fetchAllAuthor,
    deleteAuthorById,
    updateAuthorById,
    fetchAuthorById,
    fetchAuthor
} from "../service/author";
import { useHistory, useLocation } from "react-router";
import query from 'query-string'

export default function Tablem() {
    const [author, setAuthor] = useState([]);
    const [name, setname] = useState(' ')
    const [email, setemail] = useState(' ')
    const [name1, setname1] = useState(' ')
    const [email1, setemail1] = useState(' ')
    const [myImage, setMyImage] = useState(null);
    const history = useHistory();
    const [imageFile, setImageFile] = useState(null)
    const [imageURL, setImageURL] = useState('../Image/placeholder-image.png')
    const [checkClear, setCheckClear] = useState(' ')
    const [checkAdd, setcheckAdd] = useState(' ')

    const onAdd = async (e) => {
        e.preventDefault()
        let author = {
            name, email
        }

        if (imageFile) {
            let url = await uploadImage(imageFile)
            author.image = url
        }

        postAuthor(author).then(message => alert(message))
        clear();
    }
    const { search } = useLocation();
    const { id } = query.parse(search);

    function onBrowsedImage(e) {
        let url = URL.createObjectURL(e.target.files[0]);
        setMyImage(url);
        setImageFile(e.target.files[0]);
    }


    useEffect(async () => {

        if (search !== " ") {
            const result = await fetchAllAuthor(id);
            setname(result.name);
            setemail(result.email);
            setMyImage(result.image);
        } else {
            setname("");
            setemail("");
            setMyImage("");
        }

    }, [search]);

    async function onUpdate() {

        if (search !== " ") {
            const url = myImage && await uploadImage(myImage)
            const author = {
                name,
                email,
                image: url ? url : myImage,
            };
            const result = await updateAuthorById(id, author)

        } else {
            const url = myImage && await uploadImage(myImage);
            const author = {
                name,
                email,
                image: url ? url : myImage,
            };
            const result = await fetchAuthorById(author);
        }
    }

    useEffect(async () => {
        const result = await fetchAllAuthor();
        console.log("Author:", result);
        setAuthor(result);
        setCheckClear(false)
        setcheckAdd(false)
    }, [checkAdd]);

    useEffect(() => {
        const fetch = async () => {
            let author = await fetchAuthor();
            setAuthor(author);
        };
        fetch();
    }, []);

    async function onDeleteAuthorById(id) {
        const result = await deleteAuthorById(id);
        const temp = author.filter(item => {
            return item._id != id
        })
        setAuthor(temp)
    }

    function clear() {
        setCheckClear(true)
        setcheckAdd(true)
        setname('')
        setemail('')
        setMyImage('../Image/placeholder-image.png')
    }

    return (
        <div>
            <br />
            <h1>Author</h1>
            <br />
            <Row>
                <Col xs lg="9">
                    <Form>
                        <Form.Group controlId="name">
                            <Form.Label>Author Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Author Name"
                                value={name}
                                onChange={(e) => setname(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group controlId="email">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Email"
                                value={email}
                                onChange={(e) => setemail(e.target.value)} />
                        </Form.Group>
                        <Button
                            variant="primary"
                            type="submit"
                            onClick={onAdd}
                        >
                            Add
                        </Button>
                        {/* <Button
                            onClick={onAddAuthor}
                            className="my-3"
                            variant="primary"
                            type="button"
                        >
                            {search?"Update":"Add"}
                        </Button> */}
                    </Form>
                </Col>
                <Col md={3}>
                    <div style={{ width: "255px" }}>
                        <label htmlFor="myfile">
                            <img width="100%" src={myImage ? myImage : imageURL} />
                        </label>
                    </div>
                    <input
                        onChange={(e) => onBrowsedImage(e)}
                        id="myfile"
                        type="file"
                        style={{ display: "" }}
                    />
                </Col>
            </Row>
            <br />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    {author.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{item._id.slice(0, 8)}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td><img width="100px" src={item.image} /></td>
                                <td>
                                    <Button onClick={() => history.push(`/author?id=${item._id}`)} variant="warning">Edit</Button>{' '}
                                    <Button onClick={() => onDeleteAuthorById(item._id)} variant="danger">Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>

            </Table>
        </div>
    )
}
