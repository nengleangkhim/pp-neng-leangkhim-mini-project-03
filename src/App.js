import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react'
import Home from './components/Home'
import Author from './components/Table'
import NavBar from './components/NavBar'
import AddArticle from './components/AddArticle'
import ViewArticle from './components/View'
import EditArticle from './components/Edit'
import { Container } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
export default class App extends Component {
    render() {
        return (
                <Router>            
                    <div>         
                    <NavBar/>
                        <Container>
                            <Switch>
                                <Route exact path="/Home" component={Home}/>
                                <Route path="/add" component={AddArticle}/>
                                <Route path="/author" component={Author}/>
                                <Route path='/article/:id' component={ViewArticle} />
                                <Route path='/update/article/:id' component={EditArticle} />
                            </Switch>
                        </Container>
                    </div>
                </Router>
        )
    }
}
