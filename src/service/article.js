import axios from "axios";
export const API = axios.create({
    baseURL: "http://110.74.194.124:3034/api",
  });
  
export const fetchArticle = async () => {
    let response = await API.get('articles')
    return response.data.data
}

export const deleteArticle = async (id) => {
    let response = await API.delete('articles/' + id)
    return response.data.message
}

export const postArticle = async (article) => {
    let response = await API.post('articles', article)
    return response.data.message
}

export const fetchArticleById = async (id) => {
    let response = await API.get('articles/' + id)
    return response.data.data
}

export const updateArticleById = async (id, newArticle) => {
    let response = await API.patch('articles/' + id, newArticle)
    return response.data.message
}
export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('images', formData)
    return response.data.url
}
export const fetchCategory = async () => {

    let response = await API.get('category')

    return response.data.data

}