import axios from "axios";
//Base URL
export const API = axios.create({
  baseURL: "http://110.74.194.124:3034/api",
});

export const postAuthor= async(author)=>{
    let response = await API.post('author',author)
    return response.data.message
}

export const uploadImage = async (file) => {

    let formData = new FormData()
    formData.append('image', file)

    let response = await API.post('images', formData)
    return response.data.url
}
export const fetchAuthorById = async(id)=>{
    try {
      const result  = await API.get(`/author/${id}`)
      console.log("fetchAuthoreById:", result.data.data);
      return result.data.data
    } catch (error) {
      console.log("fetchAuthorById error:", error);
    }
  }
  
export const deleteAuthorById = async(id) => {
    try {
        const result = await API.delete(`/author/${id}`);
        console.log("deleteAuthorById:", result.data.data);
        return result.data.data;
      } catch (error) {
        console.log("deleteAuthorById Error:", error);
      }
}

export const fetchAllAuthor = async () => {
    try {
      const result = await API.get("/author");
      console.log("fetchAllAuthor:", result.data.data);
      return result.data.data;
    } catch (error) {
      console.log("fetchAllAuthor Error:", error);
    }
  };

  export const updateAuthorById = async(id, author)=>{
    try {
      const result = await API.put(`/author/${id}`, author)
      return result.data.data
    } catch (error) {
      console.log("updateAuthorById Error:", error);
    }
  }
  

  export const fetchAuthor = async () => {
    let response = await API.get('author')
    return response.data.data
}
